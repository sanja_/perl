#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

open Code_file,"<points.java" or die "Ошибка открытия файла points.java: $!";
open Result_file,">result.txt" or die "Ошибка открытия файла result.txt: $!";

@_=<Code_file>;
close Code_file or die $!;

$_=join" ",@_;

$A =tr/\n" "\t\r//d;

#print "\n",$A;
print Result_file "$_" ,"\n" ;
close Result_file or die $!;
