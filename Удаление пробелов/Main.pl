#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

open Code_file,"<points.java" or die "Ошибка открытия файла points.java: $!";
open Result_file,">result.txt" or die "Ошибка открытия файла result.txt: $!";

@_=<Code_file>;
close Code_file or die $!;

$_=join" ",@_;

$a =tr/\n" "\t\r//d;

print $a;
print "\n",$_;
print Result_file "$_" ,"\n" ;
close Result_file or die $!;
